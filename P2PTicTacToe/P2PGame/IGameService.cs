﻿using System.ServiceModel;

namespace P2PGame
{
    [ServiceContract(SessionMode =SessionMode.Required)]
    public interface IGameService
    {
        [OperationContract(IsOneWay = true)]
        void MakeStep(GameState gameState);

        [OperationContract(IsOneWay = true)]
        void Completion(string result);
    }
}
