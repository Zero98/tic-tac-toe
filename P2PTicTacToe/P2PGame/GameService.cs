﻿using System;
using System.ServiceModel;

namespace P2PGame
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class GameService : IGameService
    {
        private Action<byte, byte> updateAction;
        private Action<string> lastAction;

        public GameService(Action<byte, byte> _updateAction, Action<string> _lastAction)
        {
            updateAction = _updateAction;
            lastAction = _lastAction;
        }

        public void Completion(string result)
        {
            lastAction(result);
        }

        public void MakeStep(GameState gameState)
        {
            updateAction(gameState.Row, gameState.Column);
        }
    }
}
