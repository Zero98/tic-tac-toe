﻿using System.Runtime.Serialization;

namespace P2PGame
{
    [DataContract]
    public class GameState
    {
        [DataMember]
        public byte Row { get; set; }
        [DataMember]
        public byte Column { get; set; }

        public GameState() { }
        public GameState(byte row, byte column) : base()
        {
            Row = row;
            Column = column;
        }
    }
}
