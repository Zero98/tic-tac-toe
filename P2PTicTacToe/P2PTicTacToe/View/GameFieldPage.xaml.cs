﻿using System.Windows.Controls;
using P2PTicTacToe.ViewModel.PageViewModel;
using P2PTicTacToe.ViewModel;
using System.Windows;
using System;
using GameComponents;

namespace P2PTicTacToe.View
{       
    public partial class GameFieldPage : Page
    {
        private GameFieldViewModel gameFieldPageVM;

        public GameFieldPage(UserViewModel userViewModel, 
            GameField gameField, 
            UserViewModel enemyViewModel = null)
        {
            InitializeComponent();
            gameFieldPageVM = new GameFieldViewModel(
                userViewModel,
                enemyViewModel,
                gameField,
                ApplyEnemyMove,
                Complete
                );
            gameFieldPageVM.GameEnded += Complete;

            Field.ItemsSource = gameFieldPageVM.GameField;
            DataContext = gameFieldPageVM;
        }
        
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                gameFieldPageVM.Connection();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "WCF Error",
                    MessageBoxButton.OK, MessageBoxImage.Stop);
            }
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            gameFieldPageVM.Disconnection();
        }

        private void Make_Step(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button.IsEnabled)
            {
                button.Content = gameFieldPageVM.UserViewModel.Side;
                button.IsEnabled = false;
                gameFieldPageVM.IsYourProcess = false;
                gameFieldPageVM.MakeStep(button.TabIndex);
            }
        }

        private void ApplyEnemyMove(byte row, byte column)
        {
            gameFieldPageVM.UpdateField(row, column);
            
            gameFieldPageVM.IsYourProcess = true;
        }

        private void Complete(string result)
        {
           Dispatcher.Invoke(() => Field.IsEnabled = false);
            MessageBox.Show(result, "Результат игры",
                    MessageBoxButton.OK, MessageBoxImage.Information);
            Dispatcher.Invoke(() => NavigationService.GoBack());
        }
    }
}
