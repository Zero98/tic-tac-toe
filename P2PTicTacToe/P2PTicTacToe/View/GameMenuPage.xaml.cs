﻿using System.Windows.Controls;
using P2PTicTacToe.ViewModel.PageViewModel;
using P2PTicTacToe.ViewModel;
using GameComponents;

namespace P2PTicTacToe.View
{
    public partial class GameMenuPage : Page
    {
        private GameMenuPageViewModel gameMenuPageViewModel;

        public GameMenuPage(UserViewModel userViewModel)
        {
            InitializeComponent();
            gameMenuPageViewModel = new GameMenuPageViewModel(userViewModel);
            DataContext = gameMenuPageViewModel;
        }

        private void BotPlayBtn_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var gameFieldPage = new GameFieldPage(gameMenuPageViewModel.UserViewModel, GameField.Small);
            NavigationService.Navigate(gameFieldPage);
        }

        private void PlayerPlayBtn_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var lobbyPage = new LobbyPage(gameMenuPageViewModel.UserViewModel);
            NavigationService.Navigate(lobbyPage);
        }
    }
}
