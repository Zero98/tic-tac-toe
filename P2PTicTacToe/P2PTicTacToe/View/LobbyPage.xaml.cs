﻿using System;
using System.ServiceModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using P2PLib;
using P2PTicTacToe.ViewModel;
using P2PTicTacToe.ViewModel.PageViewModel;
using GameComponents;

namespace P2PTicTacToe.View
{
    public partial class LobbyPage : Page
    {
        private LobbyViewModel lobbyViewModel;

        public LobbyPage(UserViewModel userViewModel)
        {
            InitializeComponent();
            lobbyViewModel = new LobbyViewModel(userViewModel, DisplayMessage);
            lobbyViewModel.AddUpdatedEventHandler(ChangCollection);
            DataContext = lobbyViewModel;
        }
        
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            { 
                lobbyViewModel.Connection();
                lobbyViewModel.Update.ExecuteAsync(null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "WCF Error",
                    MessageBoxButton.OK, MessageBoxImage.Stop);
            }
        }
      
        private void ChangCollection()
        {
            PeerList.Items.Clear();
            if(lobbyViewModel.PeerEntries.Count != 0)
            {
                StatusLabel.Visibility = Visibility.Hidden;
                foreach (var peer in lobbyViewModel.PeerEntries)
                {
                    PeerList.Items.Add(peer);
                }
            }
            else
            {
                StatusLabel.Visibility = Visibility.Visible;
            }   
        }

        private bool DisplayMessage(PeerEntry peerEntry, GameField gameField)
        {
             var result = MessageBox.Show(
                 $"Пользователь {peerEntry.Username} приглашает вас в игру",
                $"Приглашение в игру",
                MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.Cancel);
            if(result == MessageBoxResult.OK)
            {
                lobbyViewModel.EndpointUrl = peerEntry.EndpointUrl;
                lobbyViewModel.Side = peerEntry.EnemySide;
                UserViewModel EnemyViewModel = new UserViewModel(
                    peerEntry.Username,
                    peerEntry.YourEndpointUrl,
                    lobbyViewModel.GetAnotherSide());
                
                var gamefieldPage = new GameFieldPage(lobbyViewModel.UserViewModel, gameField,EnemyViewModel);
                NavigationService.Navigate(gamefieldPage);
                
            }
            return result == MessageBoxResult.OK;
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            lobbyViewModel.Disconnection();
        }

        private void ConnectionBtn3_Click(object sender, RoutedEventArgs e)
        {
            PeerEntry peerEntry = ((Button)e.OriginalSource).DataContext as PeerEntry;

            StartGame(peerEntry, GameField.Small);
        }

        private void ConnectionBtn4_Click(object sender, RoutedEventArgs e)
        {
            PeerEntry peerEntry = ((Button)e.OriginalSource).DataContext as PeerEntry;

            StartGame(peerEntry, GameField.Large);
        }

        private void StartGame(PeerEntry peerEntry, GameField gameField)
        {
            if (peerEntry != null &&
                peerEntry.ServiceProxy != null)
            {
                peerEntry.EnemySide = lobbyViewModel.GetEnemySide();

                try
                {
                    if (peerEntry.ServiceProxy.SendRequest(peerEntry, gameField))
                    {
                        lobbyViewModel.EndpointUrl = peerEntry.YourEndpointUrl;
                        UserViewModel EnemyViewModel = new UserViewModel(
                            peerEntry.DisplayString,
                            peerEntry.EndpointUrl,
                            peerEntry.EnemySide);

                        var gamefieldPage = new GameFieldPage(lobbyViewModel.UserViewModel, gameField, EnemyViewModel);

                        NavigationService.Navigate(gamefieldPage);
                    }
                }
                catch (CommunicationException) { }
            }
        }
    }
}
