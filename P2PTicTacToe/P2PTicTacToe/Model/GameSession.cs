﻿using P2PGame;
using System;
using System.ServiceModel;
using System.Threading.Tasks;

namespace P2PTicTacToe.Model
{
    public class GameSession
    {
        private const string GAMESERVICE = "GameService";
        private ServiceHost host;
        private GameService gameService;
        private string enemyUrl;
        private string myUrl;
       
        public IGameService IGameService { get; private set;}

        public GameSession(string _enemyUrl, string _myUrl, 
            Action<byte, byte> action,
            Action<string> lastAction)
        {
            enemyUrl = _enemyUrl + GAMESERVICE;
            myUrl = _myUrl + GAMESERVICE;
            gameService = new GameService(action, lastAction);
        }

        public async Task ActivateAsync()
        {
            await Task.Run(()=>
            {
                host = new ServiceHost(gameService, new Uri(myUrl));
                NetTcpBinding binding = new NetTcpBinding();
                binding.Security.Mode = SecurityMode.None;
                host.AddServiceEndpoint(typeof(IGameService), binding, myUrl);
                host.Open();
                IGameService = ChannelFactory<IGameService>.CreateChannel(
                        binding, new EndpointAddress(enemyUrl));
            });
        }

        public async Task DeactivateAsync() =>
            await Task.Run(() =>
            {
                host.Close();
            });
    }
}
