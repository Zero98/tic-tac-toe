﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace P2PTicTacToe.Model
{
    public class Cell : INotifyPropertyChanged
    {
        private int id;
        private bool isEnable;
        private char _value;

        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// Element number
        /// </summary>
        public int ID
        {
            get { return id; }
            set
            {
                id = value;
                OnPropertyChanged("ID");
            }
        }
        /// <summary>
        /// Element Availability
        /// </summary>
        public bool IsEnable
        {
            get { return isEnable; }
            set
            {
                isEnable = value;
                OnPropertyChanged("IsEnable");
            }
        }
        /// <summary>
        /// Element Value
        /// </summary>
        public char Value
        {
            get { return _value; }
            set
            {
                _value = value;
                OnPropertyChanged("Value");
            }
        }

        private void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
