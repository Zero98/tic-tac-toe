﻿using System.Linq;

namespace P2PTicTacToe.Model
{
    public static class Result
    {
        public static bool CheckWinner(Cell[][] field, int size) => 
            CheckeLines(field, size, 'X') || CheckeLines(field, size, 'O');

        public static bool CheckDraw(Cell[][] field) => field.All(n => n.All(k => k.Value != ' '));

        private static bool CheckDiagonal(Cell[][] field, int size, char symbol) 
        {
            bool toright = true, toleft = true;
            for(int i = 0; i < size; i++)
            {
                toright &= field[i][i].Value == symbol;
                toleft &= field[size - i - 1][i].Value == symbol;
            }
            return toright || toleft;
        }

        private static bool CheckeLines(Cell[][] field, int size, char symbol)
        {
            bool cols, rows, diagonal;
            diagonal = CheckDiagonal(field, size, symbol);
            for (int col = 0; col < size; col++)
            {
                cols = true;
                rows = true;
                for (int row = 0; row < size; row++)
                {
                    cols &= (field[col][row].Value == symbol);
                    rows &= (field[row][col].Value == symbol);
                }

                if (cols || rows || diagonal) return true;
            }

            return false;
        }
    }
}
