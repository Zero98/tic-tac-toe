﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using GameComponents;

namespace P2PTicTacToe.Model
{
    class Game : INotifyPropertyChanged
    {
        public const char X = 'X';
        public const char O = 'O';
        private const string YOURPROCESS = "Ваш ход";
        private const string ENEMYPROCESS = "Ход противника";
        private GameField fieldSize;
        private Cell[][] gameField;
        private string status;
        private bool isYourProcess;

        public string Status
        {
            get
            {
                status = isYourProcess ? YOURPROCESS : ENEMYPROCESS;
                return status;
            }
            set
            {
                status = value;
                OnPropertyChanged("Status");
            }
        }

        public bool IsYourProcess
        {
            get { return isYourProcess; }
            set
            {
                isYourProcess = value;
                Status = isYourProcess ? YOURPROCESS : ENEMYPROCESS;
                OnPropertyChanged("IsYourProcess");
            }
        }

        public int FieldSize() => (int)fieldSize;

        public Cell[][] GameField
        {
            get { return gameField; }
            set
            {
                gameField = value;
                OnPropertyChanged("GameField");
            }
        }

        public bool IsWinner() => Result.CheckWinner(gameField, (int)fieldSize);
        public bool IsDraw() => Result.CheckDraw(gameField);

        public event PropertyChangedEventHandler PropertyChanged;


        public Game(GameField _fieldSize)
        {
            fieldSize = _fieldSize;
            if(fieldSize == GameComponents.GameField.Small)
            {
                CreateField(3);
            }
            else
            {
                CreateField(4);
            }
        }
        
        private void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        private void CreateField(int size)
        {
            var count = 0;
            var cellsList = new List<Cell[]>();
            for (int i = 0; i < size; i++)
            {
                cellsList.Add(new Cell[size]);
                for (int j = 0; j < size; j++)
                    cellsList[i][j] = new Cell() { ID = count++, IsEnable = true, Value = ' ' };
            }
            gameField = cellsList.ToArray();
        }
    }
}
