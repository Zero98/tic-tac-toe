﻿using System;
using System.Net.Sockets;
using System.ServiceModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Net;
using System.Net.PeerToPeer;
using System.Collections.ObjectModel;
using P2PLib;
using System.Threading.Tasks;

namespace P2PTicTacToe.Model
{
    public class Peer : INotifyPropertyChanged
    {
        private const string P2PSERVICE = "P2PService";

        private string serviceUrl;
        private string machineName;
        private string username;
        private int port;
        private ServiceHost host;
        private P2PService localService;
        private PeerName peerName;
        private PeerNameRegistration peerNameRegistration;
        private PeerNameResolver resolver;
        private ObservableCollection<PeerEntry> peerEntries;
        private bool isExecuted;

        public event PropertyChangedEventHandler PropertyChanged;
        public event Action Updated;

        public ObservableCollection<PeerEntry> PeerEntries
        {
            get { return peerEntries; }
            set
            {
                peerEntries = value;
                OnPropertyChanged("PeerEntries");
            }
        }

        public Peer(string _username, P2PService service)
        {
            localService = service;
            port = FindFreePort();
            username = _username;
            machineName = Environment.MachineName;
            PeerEntries = new ObservableCollection<PeerEntry>();
            resolver = new PeerNameResolver();
            resolver.ResolveProgressChanged +=
                            new EventHandler<ResolveProgressChangedEventArgs>(Resolver_ResolveProgressChanged);
            resolver.ResolveCompleted +=
                new EventHandler<ResolveCompletedEventArgs>(Resolver_ResolveCompleted);
        }

        //Registration and launch of the service
        public void Activate()
        {
            serviceUrl = GetServiceUrl();
            host = new ServiceHost(localService, new Uri(serviceUrl + P2PSERVICE));
            NetTcpBinding binding = new NetTcpBinding();
            binding.Security.Mode = SecurityMode.None;
            host.AddServiceEndpoint(typeof(IP2PService), binding, serviceUrl + P2PSERVICE);
            host.Open();
            peerName = new PeerName("P2P Sample", PeerNameType.Unsecured);
            peerNameRegistration = new PeerNameRegistration(peerName, port);
            peerNameRegistration.Cloud = Cloud.AllLinkLocal;
            peerNameRegistration.Start();
        }

        public void Update()
        {
            PeerEntries = new ObservableCollection<PeerEntry>();
            // Преобразование незащищенных имен пиров асинхронным образом
            resolver.ResolveAsync(new PeerName("0.P2P Sample"), 1);
        }

        //Stop Registration
        public async Task Deactivate()
        {
            await Task.Run(() =>
            {
                peerNameRegistration.Stop();
                host.Close();
            });
        }

        private string GetServiceUrl()
        {
            foreach (IPAddress address in Dns.GetHostAddresses(Dns.GetHostName()))
            {
                if (address.AddressFamily == AddressFamily.InterNetwork)
                {
                    return $"net.tcp://{address}:{port}/";
                }
            }
            throw new Exception("Не удается определить адрес конечной точки WCF.");
        }
        
        private void Resolver_ResolveCompleted(object sender, ResolveCompletedEventArgs e)
        {
            if(Updated != null)
            {
                Updated();
            }
            isExecuted = false;
        }

        private async void Resolver_ResolveProgressChanged(object sender, ResolveProgressChangedEventArgs e)
        {
            PeerNameRecord peer = e.PeerNameRecord;

            foreach (IPEndPoint ep in peer.EndPointCollection)
            {
                
                if (ep.Address.AddressFamily == AddressFamily.InterNetwork)
                {
                    var enemyBaseEndpointUrl = $"net.tcp://{ep.Address}:{ep.Port}/";
                    var binding = new NetTcpBinding();
                    binding.Security.Mode = SecurityMode.None;
                    IP2PService serviceProxy = ChannelFactory<IP2PService>.CreateChannel(
                        binding, new EndpointAddress(enemyBaseEndpointUrl + P2PSERVICE));
                    await Task.Run(() =>
                    {
                        isExecuted = true;
                        try
                        {
                            PeerEntries.Add(
                                new PeerEntry
                                {
                                    PeerName = peer.PeerName,
                                    ServiceProxy = serviceProxy,
                                    DisplayString = serviceProxy.GetName(),
                                    EndpointUrl = enemyBaseEndpointUrl,
                                    YourEndpointUrl = serviceUrl,
                                    Username = username,                            
                                });
                        }
                        catch (EndpointNotFoundException) { }
                    });
                }
            }
            if (Updated != null)
            {
                Updated();
            }
        }

        private int FindFreePort()
        {
            int port = 0;

            IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, 0);
            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                socket.Bind(endPoint);
                IPEndPoint local = (IPEndPoint)socket.LocalEndPoint;
                port = local.Port;
            }

            return port;
        }

        private void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}