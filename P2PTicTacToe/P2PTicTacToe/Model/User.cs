﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace P2PTicTacToe.Model
{
    public class User : INotifyPropertyChanged
    {
        private string name;
        private string port;

        public event PropertyChangedEventHandler PropertyChanged;

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        public string EndpointUrl { get; set; }

        public char Side { get; set; }

        public User() { }

        public User(string _name, string endpointUrl, char side) : base()
        {
            name = _name;
            EndpointUrl = endpointUrl;
            Side = side;
        }

        private void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
