﻿using System;
using System.Threading.Tasks;

namespace P2PTicTacToe.Model.CommandModel
{
    /// <summary>
    /// Тип асинхронной команды
    /// </summary>
    public class AsyncCommand : AsyncCommandBase
    {
        //Асинхронный делегат, возвращающий void
        private readonly Func<Task> command;
        public AsyncCommand(Func<Task> _command)
        {
            command = _command;
        }

        public override bool CanExecute(object parameter) => true;
        public override Task ExecuteAsync(object parameter) => command();
    }
}
