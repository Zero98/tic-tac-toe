﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace P2PTicTacToe.Model.CommandModel
{
    public interface IAsyncCommand : ICommand
    {
        /// <summary>
        /// Асинхнорнный метод, который содержит логику асинхронной команды
        /// Будет вызываться внутри метода Execute
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        Task ExecuteAsync(object parameter);
    }
}
