﻿using System;
using System.Windows;
using System.Windows.Controls;
using P2PTicTacToe.ViewModel.PageViewModel;

namespace P2PTicTacToe.View
{
    public partial class MainPage : Page
    {
        private EnterPageViewModel enterPageViewModel;
        public MainPage()
        {
            InitializeComponent();
            enterPageViewModel = new EnterPageViewModel();
            DataContext = enterPageViewModel;
        }

        private void ReadyBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(LoginTextBox.Text))
            {
                var gamePage = new GameMenuPage(enterPageViewModel.UserViewModel);
                NavigationService.Navigate(gamePage);
            }
        }
    }
}
