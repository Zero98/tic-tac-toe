﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using P2PTicTacToe.Model;

namespace P2PTicTacToe.ViewModel
{
    public class UserViewModel : INotifyPropertyChanged
    {
        private User user;

        public event PropertyChangedEventHandler PropertyChanged;

        public string Name
        {
            get { return user.Name; }
            set
            {
                user.Name = value;
                OnPropertyChanged("Name");
            }
        }


        public char Side
        {
            get { return user.Side; }
            set
            {
                user.Side = value;
                OnPropertyChanged("Side");
            }
        }

        public string EndpointUrl
        {
            get { return user.EndpointUrl; }
            set
            {
                user.EndpointUrl = value;
                OnPropertyChanged("EndpointUrl");
            }
        }

        public UserViewModel()
        {
            user = new User();
        }
        
        public UserViewModel(string name, string endpoint, char side)
        {
            user = new User(name, endpoint, side);
        } 

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
