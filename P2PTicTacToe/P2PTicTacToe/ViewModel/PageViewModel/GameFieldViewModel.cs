﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using P2PGame;
using P2PLib;
using P2PTicTacToe.Model;
using GameComponents;

namespace P2PTicTacToe.ViewModel.PageViewModel
{
    public class GameFieldViewModel : INotifyPropertyChanged
    {
        private UserViewModel userViewModel;
        private UserViewModel enemyViewModel;
        private PeerEntry peerEntry;
        private Game game;
        private GameSession session;

        public event PropertyChangedEventHandler PropertyChanged;
        public event Action<string> GameEnded;
        //Prop
        /// <summary>
        /// Determination of the order of turn
        /// </summary>
        public bool IsYourProcess
        {
            get { return game.IsYourProcess; }
            set
            {
                game.IsYourProcess = value;
                OnPropertyChanged("Status");
                OnPropertyChanged("IsYourProcess");
            }
        }
        /// <summary>
        /// The current state of the game. The order of progress in text form
        /// </summary>
        public string Status
        {
            get { return game.Status; }
            set
            {
                game.Status = value;
                OnPropertyChanged("Status");
            }
        }
        /// <summary>
        /// User information
        /// </summary>
        public UserViewModel UserViewModel
        {
            get { return userViewModel; }
            set
            {
                userViewModel = value;
                OnPropertyChanged("UserViewModel");
            }
        }
        /// <summary>
        /// Information about his opponent
        /// </summary>
        public UserViewModel EnemyViewModel
        {
            get { return enemyViewModel; }
            set
            {
                enemyViewModel = value;
                OnPropertyChanged("EnemyViewModel");
            }
        }

        public Cell[][] GameField
        {
            get { return game.GameField; }
            set
            {
                game.GameField = value;
                OnPropertyChanged("GameField");
            }
        }
        //Prop
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_userViewModel">User information</param>
        /// <param name="_enemyViewModel">Information about his opponent</param>
        /// <param name="gameField">Variant of the playing field</param>
        /// <param name="action">An action that updates the playing field after the opponent's move</param>
        /// <param name="lastAction">The action to be triggered in the event of a game finish</param>
        public GameFieldViewModel(
            UserViewModel _userViewModel, 
            UserViewModel _enemyViewModel,
            GameField gameField,
            Action<byte, byte> updateAction,
            Action<string> lastAction)
        {
            userViewModel = _userViewModel;
            enemyViewModel = _enemyViewModel;
            game = new Game(gameField);
            session = new GameSession(
                enemyViewModel.EndpointUrl, 
                userViewModel.EndpointUrl,
                updateAction, lastAction);
            IdentifyInitiative();
            
        }
        //Constructor
        /// <summary>
        /// Determination of the right stroke
        /// </summary>
        public void IdentifyInitiative() => IsYourProcess = userViewModel.Side == Game.X;
        /// <summary>
        /// Create a host to play with another user
        /// </summary>
        public async void Connection() => await session.ActivateAsync();
        /// <summary>
        /// Disabling the host after the game is over
        /// </summary>
        public async void Disconnection() => await session.DeactivateAsync();
        /// <summary>
        /// Make a move
        /// </summary>
        /// <param name="tabIndex">The number of the cell into which the move is made</param>
        public void MakeStep(int tabIndex)
        {
            var row = (byte)(tabIndex / game.FieldSize());
            var column = (byte)(tabIndex % game.FieldSize());
            GameField[row][column].Value = userViewModel.Side;
            if (game.IsDraw())
            {
                session.IGameService.Completion($"Ничья!");
                GameEnded($"Ничья!");
            }

            session.IGameService.MakeStep(new GameState(row, column));       
        }
        private void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
        /// <summary>
        /// Definition of the enemy
        /// </summary>
        private void SetEnemy() => enemyViewModel = new UserViewModel()
            {
                Name = peerEntry.DisplayString,
                EndpointUrl = peerEntry.EndpointUrl
            };
        /// <summary>
        /// Updating the playing field after the opponent's move
        /// </summary>
        /// <param name="row">The line number of the cell into which the move is made</param>
        /// <param name="column">The column number of the cell in which the move is made</param>
        public void UpdateField(byte row, byte column)
        {
            GameField[row][column].Value = enemyViewModel.Side;
            GameField[row][column].IsEnable = false;
            OnPropertyChanged("GameField");
            if (game.IsWinner())
            {
                session.IGameService.Completion($"Победил игрок, игравший за {enemyViewModel.Side}");
                GameEnded($"Победил игрок, игравший за {enemyViewModel.Side}");
            }
        }
        /// <summary>
        /// Specifying the user endpoint
        /// </summary>  
        /// <param name="yourEndpointUrl">User's url</param>
        public void SetMyUrl(string yourEndpointUrl) => userViewModel.EndpointUrl = yourEndpointUrl;
    }
}