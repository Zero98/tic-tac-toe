﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using P2PLib;
using P2PTicTacToe.Model.CommandModel;
using P2PTicTacToe.Model;
using GameComponents;

namespace P2PTicTacToe.ViewModel.PageViewModel
{
    public class LobbyViewModel : INotifyPropertyChanged
    {
        private UserViewModel userViewModel;
        private Peer peer;
        private P2PService service;

        public event PropertyChangedEventHandler PropertyChanged;

        public void AddUpdatedEventHandler(Action n) => peer.Updated += n;

        public IAsyncCommand Update { get; private set; }

        public UserViewModel UserViewModel
        {
            get { return userViewModel; }
            set
            {
                
                userViewModel = value;
                OnPropertyChanged("UserViewModel");
            }
        }

        public string EndpointUrl
        {
            get { return userViewModel.EndpointUrl; }
            set
            {
                userViewModel.EndpointUrl = value;
                OnPropertyChanged("EndpointUrl");
            }
        }

        public char Side
        {
            get { return userViewModel.Side; }
            set
            {
                userViewModel.Side = value;
                OnPropertyChanged("Side");
            }
        }

        public ObservableCollection<PeerEntry> PeerEntries
        {
            get { return peer.PeerEntries; }
        }

        public LobbyViewModel(UserViewModel _userViewModel, Func<PeerEntry,GameField,bool> requestFunc)
        {
            userViewModel = _userViewModel;
            service = new P2PService(userViewModel.Name, requestFunc);
            peer = new Peer(userViewModel.Name, service);
            Update = new AsyncCommand(async () => peer.Update());
        }

        public void Connection() => peer.Activate();

        public async void Disconnection() => await peer.Deactivate();

        public char GetAnotherSide() => userViewModel.Side == Game.X ? Game.O : Game.X;

        public char GetEnemySide()
        {
            var sides = new char[] { Game.X, Game.O };
            var random = new Random();
            userViewModel.Side = sides[random.Next(0, 2)];
            return GetAnotherSide();
        }

        private void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
