﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace P2PTicTacToe.ViewModel.PageViewModel
{
    public class EnterPageViewModel : INotifyPropertyChanged
    {
        private UserViewModel userViewModel;

        public event PropertyChangedEventHandler PropertyChanged;

        public UserViewModel UserViewModel
        {
            get { return userViewModel; }
            set
            {
                userViewModel = value;
                OnPropertyChanged("UserViewModel");
            }
        }

        public EnterPageViewModel()
        {
            userViewModel = new UserViewModel();
        }

        private void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
