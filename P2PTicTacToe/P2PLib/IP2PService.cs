﻿using System.ServiceModel;
using GameComponents;

namespace P2PLib
{
    [ServiceContract]
    public interface IP2PService
    {
        [OperationContract]
        string GetName();
        
        [OperationContract(IsOneWay = false)]
        [ServiceKnownType(typeof(PeerEntry))]
        bool SendRequest(PeerEntry from, GameField gameField);
    }
}
