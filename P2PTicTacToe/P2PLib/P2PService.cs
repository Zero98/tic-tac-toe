﻿using System;
using System.ServiceModel;
using GameComponents;

namespace P2PLib
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class P2PService : IP2PService
    {
        private string username;
        private Func<PeerEntry,GameField,bool> request;

        public P2PService(string _username, Func<PeerEntry,GameField,bool> Request = null)
        {
            username = _username;
            request = Request;
        }

        public string GetName()
        {
            return username;
        }

        public bool SendRequest(PeerEntry from, GameField gameField)
        {
            bool isAgree = false;
            if(request != null)
                isAgree = request(from, gameField);
            return isAgree;
        }
    }
}
