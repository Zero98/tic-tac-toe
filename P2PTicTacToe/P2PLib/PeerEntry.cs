﻿using System.Net.PeerToPeer;
using System.Runtime.Serialization;

namespace P2PLib
{
    [DataContract]
    public class PeerEntry
    {
        [DataMember]
        public PeerName PeerName { get; set; }
        public IP2PService ServiceProxy { get; set; }
        [DataMember]
        public string DisplayString { get; set; }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string EndpointUrl { get; set; }
        [DataMember]
        public string YourEndpointUrl { get; set; }
        [DataMember]
        public char EnemySide { get; set; }

        public PeerEntry() { }
    }
}